# Mycobacterial infection induces a specific human innate immune response

This repo contains the code for the following manuscript:

> Blischak, J. D. et al. Mycobacterial infection induces a specific human innate immune response. Sci. Rep. 5, 16882; doi: 10.1038/srep16882 (2015).

Other links related to this publication:

*  [Pre-print](http://biorxiv.org/content/early/2015/04/03/017483)
*  [PubMed](http://www.ncbi.nlm.nih.gov/pubmed/26586179)
*  [Journal](http://www.nature.com/articles/srep16882)
*  [GEO](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE67427)
*  [Data repository](https://bitbucket.org/jdblischak/tb-data)
*  [Paper repository](https://bitbucket.org/jdblischak/tb-paper)

## Description

We have collected gene expression data from macrophages isolated from six
individuals. They were infected with different strains of _Mycobacterium
tuberculosis_ and other bacteria (nine total "conditions" including infections
and controls). Furthermore, we collected data at 4, 18, and 48 hours
post-infection.

### Main analysis files

+ `differential-expression.Rmd` - Performs DE analysis with limma/voom.

+ `joint_analysis.Rmd` - Runs Cormotif to classify genes according to
                         their expression patterns across the
                         infections.

+ `candidate_genes.Rmd` - Plots expression of immune response genes.

+ `reQTL.Rmd` - Compares FDRs from DE analysis of response
                eQTL-associated genes (identified in 
                [Barreiro, Tailleux et al. 2012][]) to the
                background of expressed genes.

[Barreiro, Tailleux et al. 2012]: http://www.pnas.org/content/109/4/1204.short

### Preprocessing steps

Uses R package [ngspipeline][].

[ngspipeline]: https://bitbucket.org/jdblischak/ngspipeline/src

#### Sort files

```bash
./organize_files.R ../data/flow-cells.csv ../samples/
```

#### Map reads

```bash
find ../samples/ -name "*fastq.gz" -exec bash -c \
'echo "./map_fastq.R {} ~/subread-indexed-genomes/hg19/hg19" | \
qsub -l h_vmem=16g,bigio=1 -V -N map-`basename {}` -cwd -o stdout -e stderr' \;
```

#### Count reads per gene

```bash
mkdir std-counts
find ../samples/ -name "*bam" -exec bash -c \
'echo "./count_reads.R -sl ../data/gene_lengths.txt {} ../data/exons.txt -o /KG/jdblischak/counts" | \
qsub -l h_vmem=8g -V -N count-`basename {}` -cwd -o std-counts -j y' \;
```

#### Combine annotation and gene counts

```r
library(ngspipeline)
anno  <-  "../data/annotation.txt"
count_files <- list.files("/KG/jdblischak/counts/",
                          pattern = "counts$", full.name = TRUE)
out_name <- "../data/counts_per_run.txt"
add_anno_to_counts(anno, count_files, out_name)
```

#### Sum gene counts per sample

```r
library(ngspipeline)
sum_counts_per_sample("../data/counts_per_run.txt",
                      "../data/counts_per_sample.txt")
```

## License

The code is available via the [GPLv3 license](https://www.gnu.org/licenses/gpl.html) (because we use multiple R/Biocodncutor packages which are GPL'd).
The text is available via the [CC BY 3.0 license](https://creativecommons.org/licenses/by/3.0/).
